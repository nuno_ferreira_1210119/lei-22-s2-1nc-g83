package app.controller;

import app.domain.model.SNSUser;
import app.domain.stores.SNSUserStore;

public class LoadSNSUsersController {

    private App app;
    private SNSUserStore snsUserStore;

    public LoadSNSUsersController() {
        this.app = App.getInstance();
        this.snsUserStore = new SNSUserStore();
    }

    public SNSUserStore getSNSUserFromCSVFile(String filePath) {
        return null;
    }

    public SNSUserStore getSnsUserStore() {
        return snsUserStore;
    }

    public void saveSNSUser(SNSUser newSnsUser) {

    }
}
