package app.controller;

import app.domain.dtos.VaccineScheduleDto;
import app.domain.dtos.mappers.VaccineScheduleMapper;
import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineSchedule;
import app.domain.stores.VaccinationCenterStore;
import app.domain.stores.VaccineScheduleStore;
import app.domain.stores.VaccineTypeStore;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public class VaccineScheduleController {

    private App app;
    private VaccineScheduleStore vaccineScheduleStore;
    private VaccinationCenterStore vaccinationCenterStore;
    private VaccineTypeStore vaccineTypeStore;
    private VaccineScheduleMapper vsMapper;


    public VaccineScheduleController()
    {
        this.app = App.getInstance();
        this.vaccineScheduleStore = new VaccineScheduleStore();
        this.vaccinationCenterStore = new VaccinationCenterStore();
        this.vaccineTypeStore = new VaccineTypeStore();
    }

    public boolean createVaccineSchedule(String snsNumber, Integer vaccinationCenterId, Integer vaccinationTypeId, Date date, LocalDateTime time) {
        return true;
    }

    public boolean validateVaccineSchedule(VaccineSchedule vaccineSchedule) {
        return true;
    }

    public void saveVaccinationSchedule(VaccineSchedule vaccineSchedule) {
    }

    public List<VaccineScheduleDto> getVaccinationSchedules() {
        return null;
    }

    public VaccineScheduleStore getVaccineScheduleStore() {
        return vaccineScheduleStore;
    }

    public VaccinationCenterStore getVaccinationCenterStore() {
        return vaccinationCenterStore;
    }

    public VaccineTypeStore getVaccineTypeStore() {
        return vaccineTypeStore;
    }

    public VaccinationCenter getVaccinationCenter(Integer vaccinationCenterId) {
        return null;
    }

    public VaccinationCenter getVaccineType(Integer vaccineTypeId) {
        return null;
    }
}
