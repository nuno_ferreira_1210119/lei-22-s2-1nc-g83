package app.controller;

import app.domain.model.Nurse;
import app.domain.model.SNSUser;
import app.domain.model.Vaccine;
import app.domain.stores.NurseStore;
import app.domain.stores.VaccineScheduleStore;
import app.domain.stores.VaccineStore;

import java.time.LocalDateTime;
import java.util.List;

public class VaccineAdministrationController {

    public NurseStore getNurseStore() {
        return null;
    }

    public Nurse getNurseById(Integer loggedInNurse) {
        return null;
    }

    public VaccineScheduleStore getVaccineScheduleStore() {
        return null;
    }

    public List<SNSUser> getListOfSnsUsers() {
        return null;
    }

    public VaccineStore getVaccinesStore() {
        return null;
    }

    public void createVaccineList() {

    }

    public Vaccine getVaccine() {
        return null;
    }

    public void addVaccine(Vaccine vaccine) {

    }

    public LocalDateTime getDateTime() {
        return null;
    }

    public void createVaccineAdministration(LocalDateTime administrationDateTime, Integer snsUserid, Integer doseNumber, LocalDateTime nextVaccineDate) {

    }

    public void saveVaccineAdministration() {

    }
}
