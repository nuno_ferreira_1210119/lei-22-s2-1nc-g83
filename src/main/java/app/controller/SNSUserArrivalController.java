package app.controller;

import app.domain.model.SNSUser;
import app.domain.model.VaccineSchedule;
import app.domain.model.VaccineScheduleArrival;
import app.domain.stores.SNSUserStore;
import app.domain.stores.VaccineScheduleArrivalStore;
import app.domain.stores.VaccineScheduleStore;

public class SNSUserArrivalController {

    private App app;
    private SNSUserStore snsUserStore;
    private VaccineScheduleStore vaccineScheduleStore;
    private VaccineScheduleArrivalStore vaccineScheduleArrivalStore;

    public SNSUserArrivalController()
    {
        this.app = App.getInstance();
        this.snsUserStore = new SNSUserStore();
        this.vaccineScheduleStore = new VaccineScheduleStore();
        this.vaccineScheduleArrivalStore = new VaccineScheduleArrivalStore();
    }

    public SNSUser getSNSUserBySNSUserNumber(Integer snsUserNumber){
        return null;
    }

    public VaccineSchedule getVaccineScheduleById(Integer vaccineScheduleId) {
        return null;
    }

    public SNSUserStore getSNSUserStore(){
        return snsUserStore;
    }

    public VaccineScheduleStore getVaccineScheduleStore(){
        return vaccineScheduleStore;
    }

    public VaccineScheduleArrivalStore getVaccineScheduleArrivalStore(){
        return vaccineScheduleArrivalStore;
    }

    public void saveVaccineScheduleArrival(VaccineScheduleArrival vaccineScheduleArrival) {
    }
}
