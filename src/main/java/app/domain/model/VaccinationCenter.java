package app.domain.model;

import java.io.Serializable;

public class VaccinationCenter implements Serializable {

    private String designation;
    private String name;
    private String address;
    private String emailAddress;
    private Integer faxNumber;
    private String websiteAddress;
    private String openingHours;
    private Integer slotDuration;
    private Integer vaccinePerSlot;

    public VaccinationCenter(String designation, String name, String address, String emailAddress, Integer faxNumber, String websiteAddress, String openingHours, Integer slotDuration, Integer vaccinePerSlot) {
        this.designation = designation;
        this.name = name;
        this.address = address;
        this.emailAddress = emailAddress;
        this.faxNumber = faxNumber;
        this.websiteAddress = websiteAddress;
        this.openingHours = openingHours;
        this.slotDuration = slotDuration;
        this.vaccinePerSlot = vaccinePerSlot;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Integer getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(Integer faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getWebsiteAddress() {
        return websiteAddress;
    }

    public void setWebsiteAddress(String websiteAddress) {
        this.websiteAddress = websiteAddress;
    }

    public String getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(String openingHours) {
        this.openingHours = openingHours;
    }

    public Integer getSlotDuration() {
        return slotDuration;
    }

    public void setSlotDuration(Integer slotDuration) {
        this.slotDuration = slotDuration;
    }

    public Integer getVaccinePerSlot() {
        return vaccinePerSlot;
    }

    public void setVaccinePerSlot(Integer vaccinePerSlot) {
        this.vaccinePerSlot = vaccinePerSlot;
    }
}
