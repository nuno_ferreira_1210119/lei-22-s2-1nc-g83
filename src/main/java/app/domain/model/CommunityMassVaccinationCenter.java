package app.domain.model;

public class CommunityMassVaccinationCenter extends VaccinationCenter{

    public CommunityMassVaccinationCenter(String designation, String name, String address, String emailAddress, Integer faxNumber,
                                          String websiteAddress, String openingHours, Integer slotDuration, Integer vaccinePerSlot) {
        super(designation, name, address, emailAddress, faxNumber, websiteAddress, openingHours, slotDuration, vaccinePerSlot);
    }

}
