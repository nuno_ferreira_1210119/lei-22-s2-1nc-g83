package app.domain.model;

import java.io.Serializable;
import java.time.LocalDateTime;

public class VaccineScheduleArrival implements Serializable {

    private boolean didArrive;
    private LocalDateTime arrivalDateTime;

    public VaccineScheduleArrival(boolean didArrive, LocalDateTime arrivalDateTime) {
        this.didArrive = didArrive;
        this.arrivalDateTime = arrivalDateTime;
    }

    public boolean isDidArrive() {
        return didArrive;
    }

    public void setDidArrive(boolean didArrive) {
        this.didArrive = didArrive;
    }

    public LocalDateTime getArrivalDateTime() {
        return arrivalDateTime;
    }

    public void setArrivalDateTime(LocalDateTime arrivalDateTime) {
        this.arrivalDateTime = arrivalDateTime;
    }
}
