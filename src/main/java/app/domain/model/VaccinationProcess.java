package app.domain.model;

import java.io.Serializable;
import java.util.List;

public class VaccinationProcess implements Serializable {

    private String groupAge;
    private Integer dosagesNumber;
    private Double vaccineDosage;
    private List<VaccinationAdministration> administrations;

    public VaccinationProcess(String groupAge, Integer dosagesNumber, Double vaccineDosage, List<VaccinationAdministration> administrations) {
        this.groupAge = groupAge;
        this.dosagesNumber = dosagesNumber;
        this.vaccineDosage = vaccineDosage;
        this.administrations = administrations;
    }

    public String getGroupAge() {
        return groupAge;
    }

    public void setGroupAge(String groupAge) {
        this.groupAge = groupAge;
    }

    public Integer getDosagesNumber() {
        return dosagesNumber;
    }

    public void setDosagesNumber(Integer dosagesNumber) {
        this.dosagesNumber = dosagesNumber;
    }

    public Double getVaccineDosage() {
        return vaccineDosage;
    }

    public void setVaccineDosage(Double vaccineDosage) {
        this.vaccineDosage = vaccineDosage;
    }

    public List<VaccinationAdministration> getAdministrations() {
        return administrations;
    }

    public void setAdministrations(List<VaccinationAdministration> administrations) {
        this.administrations = administrations;
    }
}
