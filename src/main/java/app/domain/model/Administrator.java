package app.domain.model;

public class Administrator extends Employee{

    public Administrator(String name, String email, String password) {
        super(name, email, password);
    }
}
