package app.domain.model;

import java.io.Serializable;
import java.time.LocalDate;

public class VaccinationAdministration implements Serializable {

    private LocalDate lastAdministrationDate;
    private LocalDate nextAdministrationDate;
    private Double dosageApplied;

    public VaccinationAdministration(LocalDate lastAdministrationDate, LocalDate nextAdministrationDate, Double dosageApplied) {
        this.lastAdministrationDate = lastAdministrationDate;
        this.nextAdministrationDate = nextAdministrationDate;
        this.dosageApplied = dosageApplied;
    }

    public LocalDate getLastAdministrationDate() {
        return lastAdministrationDate;
    }

    public void setLastAdministrationDate(LocalDate lastAdministrationDate) {
        this.lastAdministrationDate = lastAdministrationDate;
    }

    public LocalDate getNextAdministrationDate() {
        return nextAdministrationDate;
    }

    public void setNextAdministrationDate(LocalDate nextAdministrationDate) {
        this.nextAdministrationDate = nextAdministrationDate;
    }

    public Double getDosageApplied() {
        return dosageApplied;
    }

    public void setDosageApplied(Double dosageApplied) {
        this.dosageApplied = dosageApplied;
    }
}
