package app.domain.model;

import app.domain.stores.*;
import pt.isep.lei.esoft.auth.AuthFacade;
import org.apache.commons.lang3.StringUtils;

import java.util.Timer;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Company {

    private String designation;
    private AuthFacade authFacade;
    private SNSUserStore snsUserStore;
    private VaccineScheduleStore vaccineScheduleStore;
    private VaccineTypeStore vaccineTypeStore;
    private VaccineScheduleArrivalStore vaccineScheduleArrivalStore;
    private VaccineStore vaccineStore;
    private NurseStore nurseStore;

    public Company(String designation)
    {
        if (StringUtils.isBlank(designation))
            throw new IllegalArgumentException("Designation cannot be blank.");

        this.designation = designation;
        this.authFacade = new AuthFacade();
        this.snsUserStore = new SNSUserStore();
        this.vaccineScheduleStore = new VaccineScheduleStore();
        this.vaccineTypeStore = new VaccineTypeStore();
        this.vaccineScheduleArrivalStore = new VaccineScheduleArrivalStore();
        this.vaccineStore = new VaccineStore();
        this.nurseStore = new NurseStore();
    }

    public String getDesignation() {
        return designation;
    }

    public AuthFacade getAuthFacade() {
        return authFacade;
    }

    public SNSUserStore getSnsUserStore() { return snsUserStore; }

    public VaccineScheduleStore getVaccineScheduleStore() { return vaccineScheduleStore; }

    public VaccineTypeStore getVaccineTypeStore() { return vaccineTypeStore; }

    public VaccineScheduleArrivalStore getVaccineScheduleArrivalStore() { return vaccineScheduleArrivalStore; }

    public VaccineStore getVaccineStore() { return vaccineStore; }

    public NurseStore getNuRSEStore() { return nurseStore; }

    public void scheduleVaccineRecordTask() { }

    public Timer create(){ return null; }
}
