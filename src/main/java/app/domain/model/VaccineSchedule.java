package app.domain.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class VaccineSchedule implements Serializable {

    private LocalDateTime scheduleDateTime;
    private Vaccine vaccine;
    private SNSUser snsUser;

    public VaccineSchedule(LocalDateTime scheduleDateTime, Vaccine vaccine, SNSUser snsUser) {
        this.scheduleDateTime = scheduleDateTime;
        this.vaccine = vaccine;
        this.snsUser = snsUser;
    }

    public void create(String snsNumber, Integer vaccinationCenterId, Integer vaccineTypeId, LocalDate date, LocalDateTime time) {
    }

    public LocalDateTime getScheduleDateTime() {
        return scheduleDateTime;
    }

    public void setScheduleDateTime(LocalDateTime scheduleDateTime) {
        this.scheduleDateTime = scheduleDateTime;
    }

    public Vaccine getVaccine() {
        return vaccine;
    }

    public void setVaccine(Vaccine vaccine) {
        this.vaccine = vaccine;
    }

    public SNSUser getSnsUser() {
        return snsUser;
    }

    public void setSnsUser(SNSUser snsUser) {
        this.snsUser = snsUser;
    }
}
