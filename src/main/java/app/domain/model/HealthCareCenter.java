package app.domain.model;

public class HealthCareCenter extends VaccinationCenter{

    public HealthCareCenter(String designation, String name, String address, String emailAddress, Integer faxNumber,
                            String websiteAddress, String openingHours, Integer slotDuration, Integer vaccinePerSlot) {
        super(designation, name, address, emailAddress, faxNumber, websiteAddress, openingHours, slotDuration, vaccinePerSlot);
    }

}
