package app.domain.model;

import java.time.LocalDate;

public class SNSUser extends User {

    private String address;
    private String sex;
    private Integer phoneNumber;
    private LocalDate birthDate;
    private Integer snsUserNumber;
    private Integer ccNumber;

    public SNSUser(String name, String email, String password, String address, String sex, Integer phoneNumber, LocalDate birthDate, Integer snsUserNumber, Integer ccNumber) {
        super(name, email, password);
        this.address = address;
        this.sex = sex;
        this.phoneNumber = phoneNumber;
        this.birthDate = birthDate;
        this.snsUserNumber = snsUserNumber;
        this.ccNumber = ccNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Integer phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getSnsUserNumber() {
        return snsUserNumber;
    }

    public void setSnsUserNumber(Integer snsUserNumber) {
        this.snsUserNumber = snsUserNumber;
    }

    public Integer getCcNumber() {
        return ccNumber;
    }

    public void setCcNumber(Integer ccNumber) {
        this.ccNumber = ccNumber;
    }
}
