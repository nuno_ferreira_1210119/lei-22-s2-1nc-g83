package app.domain.model;

import java.util.List;

public class Vaccine {

    private List<VaccinationProcess> vaccinationProcesses;
    private List<VaccineType> vaccineTypes;

    public Vaccine(List<VaccinationProcess> vaccinationProcesses, List<VaccineType> vaccineTypes) {
        this.vaccinationProcesses = vaccinationProcesses;
        this.vaccineTypes = vaccineTypes;
    }

    public List<VaccinationProcess> getVaccinationProcesses() {
        return vaccinationProcesses;
    }

    public void setVaccinationProcesses(List<VaccinationProcess> vaccinationProcesses) {
        this.vaccinationProcesses = vaccinationProcesses;
    }

    public List<VaccineType> getVaccineTypes() {
        return vaccineTypes;
    }

    public void setVaccineTypes(List<VaccineType> vaccineTypes) {
        this.vaccineTypes = vaccineTypes;
    }
}
