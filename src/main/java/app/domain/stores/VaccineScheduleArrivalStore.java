package app.domain.stores;

import app.domain.model.VaccineSchedule;
import app.domain.model.VaccineScheduleArrival;

import java.time.LocalDateTime;

public class VaccineScheduleArrivalStore {

    public boolean validateVaccineScheduleArrival(VaccineScheduleArrival vaccineScheduleArrival) {
        return true;
    }

    public boolean createVaccineScheduleArrival(VaccineSchedule vaccineSchedule, boolean didArrive, LocalDateTime arrivalDateTime) {
        return true;
    }

    public boolean addVaccineScheduleArrival(VaccineScheduleArrival vaccineScheduleArrival) {
        return true;
    }

    public LocalDateTime getCurrentDateTime() {
        return null;
    }
}
