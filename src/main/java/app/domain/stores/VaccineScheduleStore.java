package app.domain.stores;

import app.domain.model.SNSUser;
import app.domain.model.VaccineSchedule;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public class VaccineScheduleStore {

    public boolean createVaccineSchedule(String snsNumber, Integer vaccinationCenterId, Integer vaccinationTypeId, Date date, LocalDateTime time) {
        return true;
    }

    public VaccineSchedule getVaccineScheduleById(Integer vaccineScheduleId) {
        return null;
    }

    public boolean addVaccineSchedule(VaccineSchedule vs) {
        return true;
    }

    public boolean saveVaccineSchedule(VaccineSchedule vs) {
        return true;
    }

    public boolean validateVaccineSchedule(VaccineSchedule vs) {
        return true;
    }

    public List<SNSUser> getListOfSnsUsers () {
        return null;
    }

    public SNSUser getSnsUserThatArrived () {
        return null;
    }

    public void createList () {

    }

    public void addSnsUserToUL (SNSUser snsUser) {

    }

    public void createVaccineAdministration (LocalDateTime administrationDateTime, Integer snsUserId, Integer doseNumber, LocalDateTime nextVaccineDate) {

    }

    public boolean validateVaccineAdministration (LocalDate administrationDateTime, Integer snsUserId, Integer doseNumber, LocalDateTime nextVaccineDate) {
        return true;
    }

    public boolean validateVaccineAdministration () {
        return true;
    }

    public void addVaccineAdministration () {

    }

    public LocalDateTime getSmsSendDateTime (LocalDateTime administrationDateTime) {
        return null;
    }

    public void SendSMS (LocalDateTime administrationDateTime, LocalDateTime smsDateTime) {

    }
}
