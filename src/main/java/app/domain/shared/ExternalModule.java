package app.domain.shared;

import app.domain.model.SNSUser;

public interface ExternalModule {

    SNSUser getSNSUserFromCSVFile(String filePath);
    void writeToFile(String path);
}
