package app.domain.shared;

import java.time.LocalDateTime;

public interface SmsInterface {

    void sendSMS(LocalDateTime AdministrationDateTime, LocalDateTime SmsDateTime);
}
