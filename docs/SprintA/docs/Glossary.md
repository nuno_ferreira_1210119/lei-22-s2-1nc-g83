# Glossary

**Terms, Expressions and Acronyms (TEA) must be organized alphabetically.**

(To complete according the provided example)

| **_TEA_** (EN)  | **_TEA_** (PT) | **_Description_** (EN)                                           |                                       
|:------------------------|:-------------------------------------------|:----------------------------------------------------------------------|
| **Admin** | **Admin** | Acronym for _Administrator_.|
| **APP** | **APP** | Acronym for _Application_.                       |
| **Application** | **Aplicação** | Set of one or more programs to fulfill an intended purpose.|
| **AGES** | **AGES** | Acronym for _Agrupamentos de Centros de Saúde_.|
| **Algorithm** | **Algoritmo** | Is a finite sequence of well-defined instructions, typically used to solve a class of specific problems.|
| **ARS** | **ARS** | Acronym for _Administração Regional de Saúde_.   |
| **brute-force algorithm** | **Algoritimo de Força Bruta** |straightforward methods of solving a problem that rely on sheer computing power.|
| **CamelCase** | **CamelCase** |Is used to refer to variables that comply to Pascal case rules.|
| **CC** | **CC** | Acronym for _Center cordenator_.                   |
| **DGS** | **DGS** | Acronym for _Direção geral de saude_.            |
| **Digital Certificate** | **Cetificado Digital** | Is a digital document proving that a person has a full vaccination against COVID-19.|
| **EU** | **EU** | Acronym for _European Union_.                      |
| **FIFO** | **FIFO** | Acronym for _First in first out_.              |
| **IDE** | **IDE** | Acronym for _Integrated development environment_.|
| **Immunization** | **Imunização** | A process by which a person becomes protected against a disease through vaccination.|
| **IntelliJ** | **IntelliJ** | An IDE.|
| **IT** | **IT** | Acronym for _Information tecnology_.               |
| **JaCoCo** | **JaCoCo** | A plugin used to generate the coverage report.|
| **Java** | **Java** |Object-orieted programming language.            |
| **Javadoc** | **Javadoc** | Declarations and documentation comments in a set of Java source files. |
| **JavaFx** | **JavaFx** |Is an open source, client application platform for desktop, mobile and embedded systems built on Java.         |
| **JUnit 5** | **JUnit 5** | A Java framework.|
| **NetBeans** | **NetBeans** | An IDE.|
| **OO** | **OO** | Acronym for _Object-oriented_.                     |
| **SMS** | **SMS** | Acronym for _Short Message Service_.             |
| **SNS** | **SNS** | Acronym for _Sistema nacional de saúde_.         |
| **Software** | **Software** |the programs and other operating information used by a computer.|
| **SVG** | **SVG** | A Image file format.|
| **UC** | **UC** | Acronym for _Unidade Curricular_. |
