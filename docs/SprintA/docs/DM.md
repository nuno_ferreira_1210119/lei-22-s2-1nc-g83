# OO Analysis #

The construction process of the domain model is based on the client specifications, especially the nouns (for _concepts_) and verbs (for _relations_) used. 

## Rationale to identify domain conceptual classes ##
To identify domain conceptual classes, start by making a list of candidate conceptual classes inspired by the list of categories suggested in the book "Applying UML and Patterns: An Introduction to Object-Oriented Analysis and Design and Iterative Development". 


### _Conceptual Class Category List_ ###

**Business Transactions**

* Vaccination Scheduling, Vaccine Administration, Vaccination Process

---

**Transaction Line Items**

*

---

**Product/Service related to a Transaction or Transaction Line Item**

*  Vaccine, Adverse Reactions, User Presence, Vaccination Center

---


**Transaction Records**

*  SNS User Registration, Vaccine Administration Registration Data

---  


**Roles of People or Organizations**

* Administrator, Center Coordinator, SNS User, Receptionist,
  Nurse.


---


**Places**

*  Vaccination Center (Community Mass Vaccination Center,
   Health Care Center)

---

**Noteworthy Events**

* Vaccine Schedule, Vaccine Administration, Vaccination Process

---


**Physical Objects**

* Vaccine

---


**Descriptions of Things**

*  VaccineType, Vaccine Administration, Employee


---


**Catalogs**

*  Vaccine Type

---


**Containers**

*  

---


**Elements of Containers**

*  

---


**Organizations**

*  Company(DGS)

---

**Other External/Collaborating Systems**

*  SMS Notification, Email Notification 


---


**Records of finance, work, contracts, legal matters**

* 

---


**Financial Instruments**

*  

---


**Documents mentioned/used to perform some work/**

* EU Digital Certificate
* VaccinationCertificate
---

| Category                                                        | Candidate Classes                                                          | 
|-----------------------------------------------------------------|----------------------------------------------------------------------------| 
| Places                                                          | Vaccination Center (Community Mass Vaccination Center, Health Care Center) | 
| Roles of People                                                 | Administrator, Center Coordinator, SNS User, Receptionist, Nurse.          | 
| Transactions                                                    | Vaccination Scheduling, Vaccine Administration, Vaccination Process        | 
| Transaction Records/Registers                                   | SNS User Registration, Vaccine Administration Registration Data            | 
| Products or Services related to Transaction or Transaction Line | Vaccine, Adverse Reactions, User Presence, Vaccination Center              | 
| Organizations (Other)                                           | Company (DGS)                                                              | 
| Noteworthy Events                                               | Vaccine Schedule, Vaccine Administration, Vaccination Process              | 
| Description of things                                           | Vaccine Type, VaccinationAdministration, Employee                          | 
| Other (External) Systems                                        | SMS Notification, Email Notification                                       |

###**Rationale to identify associations between conceptual classes**###

An association is a relationship between instances of objects that indicates a relevant connection and that is worth of remembering, or it is derivable from the List of Common Associations: 

+ **_A_** is physically or logically part of **_B_**
+ **_A_** is physically or logically contained in/on **_B_**
+ **_A_** is a description for **_B_**
+ **_A_** known/logged/recorded/reported/captured in **_B_**
+ **_A_** uses or manages or owns **_B_**
+ **_A_** is related with a transaction (item) of **_B_**
+ etc.


| Concept (A)                     | Association                                                                                                                                               | Concept (B)                                                                                                                                                                   | 
|---------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------| 
| Administrator                   | <li> configures and manages <br><li> configures and manages<br><li> configures and manages<br><li> configures and manages<br> <li> configures and manages | <li> Employee(Administrator, Receptionist, CenterCoordinator, Nurse) <br><li> CommunityMassVaccinationCenter <br><li> HealthCareCenter <br><li> Vaccine <br> <li> VaccineType |
| Company                         | <li> manages <br/> <li> manages <br/> <li> administers                                                                                                    | <li>CommunityMassVaccinationCenter <br/> <li> HealthCareCenter <br/><li> Vaccine                                                                                              | 
| Center Coordinator              | <li> manages and monitors </br>                                                                                                                           | <li> VaccinationProcess                                                                                                                                                       | 
| CommunityMassVaccinationCenter  | <li> can administer                                                                                                                                       | <li> VaccineType                                                                                                                                                              | 
| Employee                        | <li> act as                                                                                                                                               | <li> Administrator, Receptionist, CenterCoordinator, Nurse                                                                                                                    |
| HealthCareCenter                | <li> can administer <br /> <li> is associated with                                                                                                        | <li> VaccineType <br /> <li> ARS, AGES                                                                                                                                        |
| Nurse                           | <li> delivers and issues <li> applies                                                                                                                     | <li> VaccinationCertificate <li> Vaccine                                                                                                                                      |
| Receptionist                    | <li> verifies and schedules <br> <li> records data from <br>                                                                                              | <li> VaccineSchedule <br> <li> SNSUser <br>                                                                                                                                   |
| SNS User                        | <li> can receive <br> <li> can ask for <br /> <li> can take <br /> <li> can schedule <li> has                                                             | <li> SMSNotification, EmailNotification <br> <li> VaccinationCertificate <br /> <li> Vaccine <br /> <li> VaccineSchedule <li> AdverseReactions                                |
| Vaccine                         | <li> is of <li> has                                                                                                                                       | <li> VaccineType <li> VaccinationProcess                                                                                                                                      | 
| VaccineType                     | <li> created by                                                                                                                                           | <li> Administrator                                                                                                                                                            |
| VaccineSchedule                 | <li> created by/for <br> <li> has                                                                                                                         | <li> SNSUser <br> <li> Vaccine                                                                                                                                                | 
| VaccinationCertificate          | <li> printed by <br>                                                                                                                                      | <li> Nurse <br>                                                                                                                                                               | 
| VaccinationAdministration       | <li> is of <br><li> administrated on <br><li> applied by <br><li> can generate <br><li> fulfills <br><li> is contained in <br>                            | <li> Vaccine <br><li> SNSUser <br><li> Nurse<br><li> AdverseReactions <br><li> VaccineSchedule <br> <li> VaccinationProcess                                                   | 

## Domain Model

**Do NOT forget to identify concepts attributes too.**

**Insert below the Domain Model Diagram in an SVG format**

![DM.svg](DM.svg)



