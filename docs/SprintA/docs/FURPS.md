## Functionality

_Specifies functionalities that:_

- _are common across several US/UC;_
- _are not related to US/UC, namely: Audit, Reporting and Security._



#### Auditing 
- _Recording of data regarding the vaccination process for audit purposes._

#### Localization
- _The application must support, at least, the Portuguese and the English languages._

#### Notification
- _Add services related to sending/receiving email and sending/receiving SMS._

#### Reporting
- _Support for generating reports based on vaccine administration process and vaccination center effectiveness._

#### Security
- _All those who wish to use the application must be authenticated._
- _Only the nurses are allowed to access all user’s health data._

#### Workflow
- _Support for managing the status the vaccination process and user's status(e.g, register user's arrival,
  confirm user's schedule, acknowledge user's status)._



## Usability
_Evaluates the user interface. It has several subcategories,
among them: error prevention; interface aesthetics and design; help and
documentation; consistency and standards._


(fill in here )

## Reliability
_Refers to the integrity, compliance and interoperability of the software. The requirements to be considered are: frequency and severity of failure, possibility of recovery, possibility of prediction, accuracy, average time between failures._
_The application to be operated daily by SNS users, nurses, receptionists, etc_

(fill in here )

## Performance
_Evaluates the performance requirements of the software, namely: response time, start-up time, recovery time, memory consumption, CPU usage, load capacity and application availability._


(fill in here )

## Supportability
#### Testability
_The supportability requirements gathers several characteristics, such as:
testability, adaptability, maintainability, compatibility,
configurability, instability, scalability and more._



- _All methods must implement unit tests, except for methods that implement Input/Output operations._


## +

### Design Constraints

_Specifies or constraints the system design process. Examples may include: programming languages, software process, mandatory standards/patterns, use of development tools, class library, etc._


- The application must be developed in Java language using the IntelliJ IDE or NetBeans.

- The application graphical interface is to be developed in JavaFX 11.


### Implementation Constraints

_Specifies or constraints the code or construction of a system such as: mandatory standards/patterns, implementation languages,
database integrity, resource limits, operating system._


- The application should use object serialization to ensure data persistence between two runs of the application.

- The unit tests should be implemented using the JUnit 5 framework.

- The JaCoCo plugin should be used to generate the coverage report.

- All the images/figures produced during the software development process should be recorded in SVG format.


### Interface Constraints
_Specifies or constraints the features inherent to the interaction of the
system being developed with other external systems._


(fill in here )

### Physical Constraints

_Specifies a limitation or physical requirement regarding the hardware used to house the system, as for example: material, shape, size or weight._

(fill in here )
