# Glossary

| **_TEA_** (EN)  | **_TEA_** (PT) | **_Description_** (EN)                                           |                                       
|:------------------------|:-------------------------------------------|:----------------------------------------------------------------------|
| **Admin** | **Admin** | Acronym for _Administrator_.|
| **APP** | **APP** | Acronym for _Application_.                       |
| **AGES** | **AGES** | Acronym for _Agrupamentos de Centros de Saúde_.|
| **ARS** | **ARS** | Acronym for _Administração Regional de Saúde_.   |
| **CC** | **CC** | Acronym for _Center cordenator_.                   |
| **DGS** | **DGS** | Acronym for _Direção geral de saude_.            |
| **Digital Certificate** | **Cetificado Digital** | Is a digital document proving that a person has a full vaccination against COVID-19.|
| **EU** | **EU** | Acronym for _European Union_.                      |
| **FIFO** | **FIFO** | Acronym for _First in first out_.              |
| **HCC** | **HCC** | Acronym for _Health Care Center_.                |
| **Immunization** | **Imunização** | A process by which a person becomes protected against a disease through vaccination.|
| **IT** | **IT** | Acronym for _Information tecnology_.               |
| **MVC** | **MVC** | Acronym for _Mass Vaccination Center_.           |
| **SMS** | **SMS** | Acronym for _Short Message Service_.             |
| **SNS** | **SNS** | Acronym for _Sistema nacional de saúde_.         |