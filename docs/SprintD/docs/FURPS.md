## Functionality

_Specifies functionalities that:_

- _are common across several US/UC;_
- _are not related to US/UC, namely: Audit, Reporting and Security._




#### Localization
- _The application must support, at least, the Portuguese and the English languages._


#### Security
- _All those who wish to use the application must be authenticated._


## Usability
_Evaluates the user interface. It has several subcategories, among them: error prevention; interface aesthetics and design; help and documentation; consistency and standards._

N/A


## Reliability
_Refers to the integrity, compliance and interoperability of the software. The requirements to be considered are: frequency and severity of failure, possibility of recovery, possibility of prediction, accuracy, average time between failures._
N/A

## Performance
_Evaluates the performance requirements of the software, namely: response time, start-up time, recovery time, memory consumption, CPU usage, load capacity and application availability._
N/A


## Supportability
_The supportability requirements gathers several characteristics, such as: testability, adaptability, maintainability, compatibility, configurability,  installability, scalability and more._ 

#### Testability


- _All methods must implement unit tests, except for methods that implement Input/Output operations._


## +

### Design Constraints
_Specifies or constraints the system design process. Examples may include: programming languages, software process, mandatory standards/patterns, use of development tools, class library, etc._

- The application must be developed in Java language 

- The application graphical interface is to be developed in JavaFX 11.

- The application should use object serialization to ensure data persistence between two runs of the application.

- Adopt best practices for identifying requirements, and for OO software analysis and design.


### Implementation Constraints

_Specifies or constraints the code or construction of a system such as: mandatory standards/patterns, implementation languages, database integrity, resource limits, operating system._

- Adopt recognized coding standards.

- The application must use Javadoc to generate useful documentation for Java code.

- All the images/figures produced during the software development process should be recorded in SVG format.

- The unit tests should be implemented using the JUnit 5 framework.

- The JaCoCo plugin should be used to generate the coverage report.


### Interface Constraints
_Specifies or constraints the features inherent to the interaction of the
system being developed with other external systems._


(fill in here )

### Physical Constraints

_Specifies a limitation or physical requirement regarding the hardware used to house the system, as for example: material, shape, size or weight._

(fill in here )
