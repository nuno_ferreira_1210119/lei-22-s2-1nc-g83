# Integrating Project for the 2nd Semester of LEI-ISEP 2021-22

# 1. Team Members


| Student Number	| Name |
|--------------|----------------------------|
| **1191329**  | Lucas Silva                |
| **1210119**  | Nuno Ferreira              |




# 2. Task Distribution ###

**Keep this table must always up-to-date.**

| Task                                    | [Sprint A](SprintA/README.md)   | [Sprint B](SprintB/README.md)           | [Sprint C](SprintC/README.md)           | [Sprint D](SprintD/README.md)   |
|-----------------------------------------|---------------------------------|-----------------------------------------|-----------------------------------------|---------------------------------|
| Glossary                                | [all](SprintA/docs/Glossary.md) | [all](SprintB/docs/Glossary.md)         | [all](SprintC/docs/Glossary.md)         | [all](SprintD/docs/Glossary.md) |
| Use Case Diagram (UCD)                  | [all](SprintA/docs/UCD.svg)     | [all](SprintB/docs/UCD.svg)             | [all](SprintC/UCD.md)                   | [all](SprintD/UCD.md)           |
| Supplementary Specification             | [all](SprintA/docs/FURPS.md)    | [all](SprintB/docs/FURPS.md)            | [all](SprintC/FURPS.md)                 | [all](SprintD/FURPS.md)         |
| Domain Model                            | [all](SprintA/docs/DM.md)       | [all](SprintB/docs/DM.md)               | [all](SprintC/DM.md)                    | [all](SprintD/DM.md)            |
| US 003 (Register SNS user)              |                                 | [1191329](/docs/SprintB/US003/US003.md) |                                         |                                 |
| US 009 (Register Vaccination Center)    |                                 | [1210119](/docs/SprintB/US009/US009.md) |                                         |                                 |                                 
| US 010 (Register an Employee)           |                                 | [1080989]()                             |                                         |                                 |
| US 001 (Schedule a vaccine)             |                                 |                                         | [1191329](/docs/SprintC/US001/US001.md) |                                 |
| US 004 (Register arrival of SNS User)   |                                 |                                         | [1210119](/docs/SprintC/US004/US004.md) |                                 |
| US 014 (Load set of user from CSV file) |                                 |                                         | [all](/docs/SprintC/US014/US014.md)     |                                 |
| US 006 (Load set of user from CSV file) ||                                 |                                         | [1191329](/docs/SprintD/US006/US006.md)     |                                 |
| US 008 (Load set of user from CSV file) ||                                 |                                         | [1210119](/docs/SprintD/US008/US008.md)     |                                 |