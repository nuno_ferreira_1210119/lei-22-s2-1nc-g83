# OO Analysis #

## Rationale to identify domain conceptual classes ##


### _Conceptual Class Category List_ ###

**Business Transactions**

* Vaccination 

---

**Transaction Line Items**

* Vaccine

---

**Product/Service related to a Transaction or Transaction Line Item**

* Vaccine Schedule.
* Vaccine Administration.
* Vaccination Process.

---


**Transaction Records**

* Vaccine Schedule.

---  


**Roles of People or Organizations**

* Administrator. 
* Center Coordinator. 
* SNS User.
* Receptionist.
* Nurse.


---


**Places**

*  Vaccination Center (Community Mass Vaccination Center,
   Health Care Center)

---

**Noteworthy Events**

* Vaccine Schedule.
* Vaccine Administration.
* Vaccination Process.

---


**Physical Objects**

* Vaccine

---


**Descriptions of Things**

* VaccineType.

* Employee


---


**Catalogs**

*  Vaccine Type.

---


**Containers**

*  

---


**Elements of Containers**

*  

---


**Organizations**

*  Company(DGS)

---

**Other External/Collaborating Systems**

*  SMS Notification
*  Email Notification 


---


**Records of finance, work, contracts, legal matters**

* 

---


**Financial Instruments**

*  

---


**Documents mentioned/used to perform some work/**

* EU Digital Certificate
* VaccinationCertificate

### **Rationale to identify associations between conceptual classes** ###

An association is a relationship between instances of objects that indicates a relevant connection and that is worth of remembering, or it is derivable from the List of Common Associations: 


| Concept (A)                     | Association                                                                                                                                               | Concept (B)                                                                                                                                                                   | 
|---------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------| 
| Administrator                   | <li> configures and manages <br>| <li> Employee <br><li> CommunityMassVaccinationCenter <br><li> HealthCareCenter <br> <li> VaccineType |
| Company                         | <li> manages <br/> <li> manages <br/> <li> administers                                                                                                    | <li>CommunityMassVaccinationCenter <br/> <li> HealthCareCenter <br/><li> Vaccine                                                                                              | 
| Center Coordinator              | <li> manages and monitors </br>                                                                                                                           | <li> VaccinationProcess                                                                                                                                                       | 
| CommunityMassVaccinationCenter  | <li> can administer                                                                                                                                       | <li> Vaccine                                                                                                                                                              | 
| Employee                        | <li> can be                                                                                                                                               | <li> Administrator<br/> <li> Receptionist<br/> <li> CenterCoordinator<br/> <li>Nurse <br/>                                                                                                                 |
| HealthCareCenter                | <li> can administer <br /> <li> is associated with    <br/><li> is associated with                                                                                                    | <li> Vaccine <br /> <li> ARS<br/> <li> AGES                                                                                                                                        |
| Nurse                           | <li> delivers and issues <li> applies                                                                                                                     | <li> VaccinationCertificate <li> Vaccine                                                                                                                                      |
| Receptionist                    | <li> verifies and schedules <br> <li> records data from <br>                                                                                              | <li> VaccineSchedule <br> <li> SNSUser <br>                                                                                                                                   |
| SNS User                        | <li> can receive <br> <li> can ask for <br /> <li> can take <br /> <li> can schedule <li> has                                                             | <li> SMSNotification, EmailNotification <br> <li> VaccinationCertificate <br /> <li> Vaccine <br /> <li> VaccineSchedule <li> AdverseReactions                                |
| Vaccine                         | <li> is of <li> has                                                                                                                                       | <li> VaccineType <li> VaccinationProcess                                                                                                                                      | 
| VaccineType                     | <li> created by                                                                                                                                           | <li> Administrator                                                                                                                                                            |
| VaccineSchedule                 | <li> created by/for <br> <li> has                                                                                                                         | <li> SNSUser <br> <li> Vaccine                                                                                                                                                | 
| VaccinationCertificate          | <li> printed by <br>                                                                                                                                      | <li> Nurse <br>                                                                                                                                                               | 
| VaccinationAdministration       | <li> is of <br><li> administrated on <br><li> applied by <br><li> can generate <br><li> fulfills <br><li> is contained in <br>                            | <li> Vaccine <br><li> SNSUser <br><li> Nurse<br><li> AdverseReactions <br><li> VaccineSchedule <br> <li> VaccinationProcess                                                   | 

## Domain Model

![DM.svg](DM.svg)



